﻿namespace Configuration.Watchdog.Consul
{
    internal abstract class Result
    {
        public object Value { get; protected set; }
        public bool IsError => !IsSuccess && !IsWarning;
        public bool IsSuccess { get; private set; }
        public bool IsWarning { get; private set; }
        public string Message { get; private set; }

        public static Result<T> Success<T>(T value, string message = null)
        {
            return new Result<T>
            {
                Value = value,
                Message = message,
                IsSuccess = true
            };
        }

        public static Result<T> Warning<T>(T value, string message)
        {
            return new Result<T>
            {
                Message = message,
                Value = value,
                IsWarning = true
            };
        }

        public static Result<T> Error<T>(string message)
        {
            return new Result<T> {Message = message};
        }
    }

    internal sealed class Result<T> : Result
    {
        public new T Value
        {
            get { return (T)base.Value; }
            set { base.Value = value; }
        }
    }
}
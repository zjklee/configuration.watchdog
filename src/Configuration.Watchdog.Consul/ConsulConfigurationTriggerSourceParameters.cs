﻿using System;

namespace Configuration.Watchdog.Consul
{    
    public class ConsulConfigurationTriggerSourceParameters
    {
        public string Prefix { get; set; }
        public TimeSpan RequestWaitTime { get; set; }
        public TimeSpan? RequestDelayTime { get; set; }
    }    
}
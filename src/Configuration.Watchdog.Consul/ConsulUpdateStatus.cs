﻿namespace Configuration.Watchdog.Consul
{
    internal class ConsulUpdateStatus
    {
        public ulong Index { get; set; }
        public string Key { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public bool FireUpdate { get; set; }

        public ConsulUpdateStatus()
        {
        }

        public ConsulUpdateStatus(ConsulUpdateStatus consulUpdateStatus, bool fireEvent)
        {
            Index = consulUpdateStatus.Index;
            Key = consulUpdateStatus.Key;
            NewValue = consulUpdateStatus.NewValue;
            OldValue = consulUpdateStatus.OldValue;
            FireUpdate = fireEvent;
        }
    }
}
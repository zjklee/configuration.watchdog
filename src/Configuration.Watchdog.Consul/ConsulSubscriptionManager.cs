﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Configuration.Watchdog.Abstraction;
using Consul;
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog.Consul
{
    internal class ConsulSubscriptionManager : ISubscriptionManager
    {
        private readonly IConsulClient _consulClient;
        private readonly IReadOnlyConfigurationCallbackRegistry _callbackRegistry;
        private readonly ConsulConfigurationTriggerSourceParameters _parameters;
        private readonly ILogger _logger;
        private readonly Dictionary<string, Task> _activeSubscriptions = new Dictionary<string, Task>();
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public IReadOnlyDictionary<string, Task> Subscriptions => _activeSubscriptions;


        public ConsulSubscriptionManager(ILoggerFactory loggerFactory, IConsulClient consulClient,
            IReadOnlyConfigurationCallbackRegistry callbackRegistry,
            ConsulConfigurationTriggerSourceParameters parameters)
        {
            _consulClient = consulClient;
            _callbackRegistry = callbackRegistry;
            _parameters = parameters;
            _logger = loggerFactory.CreateLogger(typeof(ConsulSubscriptionManager).Name);
        }

        public void Subscribe(string key)
        {
            var cancellationToken = _cancellationTokenSource.Token;

            var subscription = Task.Factory.StartNew(async () =>
            {
                ConsulUpdateStatus currentStatus = null;
                var consulKey = ConvertConfigurationKeyToConsulKey(key);

                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        currentStatus = await GetCurrentStatus(currentStatus, consulKey, cancellationToken)
                            .ConfigureAwait(false);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, exception.Message);
                    }

                    await Sleep(cancellationToken).ConfigureAwait(false);
                }

                if (cancellationToken.IsCancellationRequested)
                {
                    _logger.LogTrace($"Cancellation Requested. Stopping handler for key '{key}'");
                }
            }, TaskCreationOptions.LongRunning);

            _activeSubscriptions.Add(key, subscription);
        }

        private async Task Sleep(CancellationToken cancellationToken)
        {
            if (_parameters.RequestDelayTime.HasValue)
            {
                try
                {
                    await Task.Delay(_parameters.RequestDelayTime.Value, cancellationToken)
                        .ConfigureAwait(false);
                }
                catch (TaskCanceledException)
                {
                    // ignore
                }
            }
        }

        private async Task<ConsulUpdateStatus> GetCurrentStatus(ConsulUpdateStatus currentStatus, string consulKey,
            CancellationToken cancellationToken)
        {
            var result = await (
                currentStatus != null
                    ? UpdateValueFactory(consulKey, currentStatus, cancellationToken)
                    : AddValueFactory(consulKey, cancellationToken)
            ).ConfigureAwait(false);

            result = Log(result);
            result = await FireUpdateEvent(result).ConfigureAwait(false);

            return result.Value;
        }

        private async Task<Result<ConsulUpdateStatus>> FireUpdateEvent(Result<ConsulUpdateStatus> result)
        {
            if (result.IsError || result.Value == null)
            {
                return result;
            }

            var consulUpdateStatus = result.Value;
            if (!consulUpdateStatus.FireUpdate) return result;

            var callback = _callbackRegistry.GetCallback(consulUpdateStatus.Key);
            _logger.LogTrace($"Firing handler for key '{consulUpdateStatus.Key}' update");

            try
            {
                await callback.Invoke(consulUpdateStatus.Key, consulUpdateStatus.OldValue, consulUpdateStatus.NewValue)
                    .ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                _logger.LogError(0, exception, $"Callback handler failed for key '{consulUpdateStatus.Key}'");
            }

            return result;
        }

        private async Task<Result<ConsulUpdateStatus>> AddValueFactory(string key, CancellationToken cancellationToken)
        {
            _logger.LogTrace($"Initializing value for key '{key}'");

            QueryResult<KVPair> queryResult;
            try
            {
                queryResult = await _consulClient.KV
                    .Get(key, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (TaskCanceledException e)
            {
                return Result.Warning<ConsulUpdateStatus>(null, e.Message);
            }
            catch (Exception exception)
            {
                return Result.Error<ConsulUpdateStatus>(exception.Message);
            }

            if (queryResult.StatusCode == HttpStatusCode.NotFound)
            {
                return Result.Error<ConsulUpdateStatus>(
                    $"Cannot find '{key}' in Consul using provided configuration. Not able to initialize value.");
            }

            if (queryResult.StatusCode != HttpStatusCode.OK)
            {
                return Result.Error<ConsulUpdateStatus>(
                    $"Consul returned '{queryResult.StatusCode}'. Not able to initialize value.");
            }

            var valuePair = ConvertToKeyValuePair(queryResult.Response);
            var consulUpdateStatus = new ConsulUpdateStatus
            {
                FireUpdate = false,
                Key = valuePair.Key,
                Index = queryResult.Response.ModifyIndex,
                NewValue = valuePair.Value
            };

            return Result.Success(consulUpdateStatus,
                $"Value for key '{key}' initialized. Current value is {valuePair.Value}.");
        }

        private async Task<Result<ConsulUpdateStatus>> UpdateValueFactory(string key,
            ConsulUpdateStatus consulUpdateStatus, CancellationToken cancellationToken)
        {
            QueryResult<KVPair> queryResult;

            try
            {
                _logger.LogTrace($"Polling for value changes for key '{key}'");

                queryResult = await _consulClient.KV.Get(key, new QueryOptions
                {
                    WaitIndex = consulUpdateStatus.Index,
                    WaitTime = _parameters.RequestWaitTime
                }, cancellationToken).ConfigureAwait(false);
            }
            catch (TaskCanceledException e)
            {
                return Result.Warning(new ConsulUpdateStatus(consulUpdateStatus, false), e.Message);
            }
            catch (Exception exception)
            {
                return Result.Warning(new ConsulUpdateStatus(consulUpdateStatus, false),
                    $"{exception.Message} Using previous known value.");
            }

            if (queryResult.StatusCode == HttpStatusCode.NotFound)
            {
                return Result.Warning(new ConsulUpdateStatus(consulUpdateStatus, false),
                    $"Cannot find '{key}' in Consul using provided configuration. Using previous known value.");
            }

            if (queryResult.StatusCode != HttpStatusCode.OK)
            {
                return Result.Warning(new ConsulUpdateStatus(consulUpdateStatus, false),
                    $"Consul returned '{queryResult.StatusCode}'. Using previous known value.");
            }

            if (queryResult.Response.ModifyIndex == consulUpdateStatus.Index)
            {
                return Result.Success(new ConsulUpdateStatus(consulUpdateStatus, false),
                    "Previous known index and the index returned from Consul are the same. Using previous known value.");
            }

            var valuePair = ConvertToKeyValuePair(queryResult.Response);

            if (valuePair.Value == consulUpdateStatus.NewValue)
            {
                return Result.Success(
                    new ConsulUpdateStatus(consulUpdateStatus, false) {Index = queryResult.Response.ModifyIndex},
                    "Previous known value and the value returned from Consul are the same. Using previous known value."
                );
            }

            consulUpdateStatus = new ConsulUpdateStatus
            {
                FireUpdate = true,
                Key = valuePair.Key,
                Index = queryResult.Response.ModifyIndex,
                NewValue = valuePair.Value,
                OldValue = consulUpdateStatus.NewValue
            };

            return Result.Success(consulUpdateStatus,
                $"Value changed for key '{key}'. Old value is '{consulUpdateStatus.OldValue}'. New value '{consulUpdateStatus.OldValue}'.");
        }

        private Result<ConsulUpdateStatus> Log(Result<ConsulUpdateStatus> result)
        {
            if (result.IsError)
            {
                _logger.LogError(result.Message);
            }

            if (result.IsWarning)
            {
                _logger.LogWarning(result.Message);
            }

            if (result.IsSuccess && !string.IsNullOrWhiteSpace(result.Message))
            {
                _logger.LogInformation(result.Message);
            }

            return result;
        }

        private KeyValuePair<string, string> ConvertToKeyValuePair(KVPair kv)
            => new KeyValuePair<string, string>(ConvertConsulKeyToConfigurationKey(kv),
                Encoding.UTF8.GetString(kv.Value));

        private string ConvertConsulKeyToConfigurationKey(KVPair kv)
            => kv.Key
                .Remove(0, _parameters.Prefix.Length)
                .Replace('/', ':');

        private string ConvertConfigurationKeyToConsulKey(string key)
        {
            return !string.IsNullOrWhiteSpace(_parameters.Prefix)
                ? $"{_parameters.Prefix.TrimEnd('/')}/{key.Replace(':', '/')}"
                : key.Replace(':', '/');
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ConsulSubscriptionManager()
        {
            Dispose(false);
        }

        private void Dispose(bool isDispose)
        {
            if (!isDispose) return;

            _cancellationTokenSource.Cancel();
            Task.WhenAll(Subscriptions.Values).Wait();
            _activeSubscriptions.Clear();
            _cancellationTokenSource.Dispose();
            _consulClient.Dispose();
        }
    }
}
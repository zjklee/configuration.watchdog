﻿using System;
using System.Collections.Generic;
using System.Linq;
using Configuration.Watchdog.Abstraction;
using Consul;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Configuration.Watchdog.Consul
{
    public class ConsulConfigurationTriggerSourceBuilder
    {
        private ILoggerFactory _loggerFactory;
        private Func<IConsulClient> _consulClientFactory;

        private readonly LinkedList<Action<ConsulConfigurationTriggerSourceParameters>> _configurationChain;

        public ConsulConfigurationTriggerSourceBuilder()
        {
            _configurationChain = new LinkedList<Action<ConsulConfigurationTriggerSourceParameters>>();
        }

        public ConsulConfigurationTriggerSourceBuilder WithConfiguration(Action<ConsulConfigurationTriggerSourceParameters> configuration)
        {
            _configurationChain.AddLast(configuration);

            return this;
        }

        public ConsulConfigurationTriggerSourceBuilder WithLoggerFactory(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;

            return this;
        }

        public ConsulConfigurationTriggerSourceBuilder WithConsulClientFactory(Func<IConsulClient> consulClientFactory)
        {
            _consulClientFactory = consulClientFactory;

            return this;
        }        

        public ConsulConfigurationTriggerSourceBuilder WithPrefix(string prefix)
        {
            _configurationChain.AddLast(parameters => parameters.Prefix = $"{prefix.TrimEnd('/')}/");

            return this;
        }        

        public IConfigurationTriggerSource Build()
        {
            _consulClientFactory = _consulClientFactory ?? (() => new ConsulClient());
            _loggerFactory = _loggerFactory ?? NullLoggerFactory.Instance;;
            
            var parameters = _configurationChain.Aggregate(new ConsulConfigurationTriggerSourceParameters
                {
                    RequestWaitTime = TimeSpan.FromSeconds(20),
                    Prefix = string.Empty
                },
                (sourceParameters, action) =>
                {
                    action(sourceParameters);
                    return sourceParameters;
                });

            return new ConsulConfigurationTriggerSource(_consulClientFactory, _loggerFactory, parameters);
        }
    }
}
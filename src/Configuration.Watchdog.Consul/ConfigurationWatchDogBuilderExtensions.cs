﻿using System;
using Configuration.Watchdog.Abstraction;
using Consul;

namespace Configuration.Watchdog.Consul
{
    public static class ConfigurationWatchDogBuilderExtensions
    {
        public static IConfigurationWatchDogBuilder AddConsul(this IConfigurationWatchDogBuilder builder, Func<IConsulClient> consulClientFactory = null)
        {
            builder.ConfigureTriggerSources((parameters, registry) =>
            {
                registry.Add(
                    new ConsulConfigurationTriggerSourceBuilder()
                        .WithLoggerFactory(parameters.LoggerFactory)
                        .WithConsulClientFactory(consulClientFactory)
                        .Build()
                );
            });

            return builder;
        }

        public static IConfigurationWatchDogBuilder AddConsul(this IConfigurationWatchDogBuilder builder, Action<ConsulConfigurationTriggerSourceParameters> configuration, Func<IConsulClient> consulClientFactory = null)
        {
            builder.ConfigureTriggerSources((parameters, registry) =>
            {
                registry.Add(
                    new ConsulConfigurationTriggerSourceBuilder()
                        .WithLoggerFactory(parameters.LoggerFactory)
                        .WithConsulClientFactory(consulClientFactory)
                        .WithConfiguration(configuration)
                        .Build()
                );
            });

            return builder;
        }
    }
}
﻿using System;
using Configuration.Watchdog.Abstraction;
using Consul;
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog.Consul
{
    internal class ConsulConfigurationTriggerSource : IConfigurationTriggerSource
    {
        private readonly Func<IConsulClient> _consulClientFactory;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ConsulConfigurationTriggerSourceParameters _parameters;
        private ISubscriptionManager _consulSubscriptionManager;

        public ConsulConfigurationTriggerSource(
            Func<IConsulClient> consulClientFactory,
            ILoggerFactory loggerFactory,
            ConsulConfigurationTriggerSourceParameters parameters)
        {
            _consulClientFactory = consulClientFactory;
            _loggerFactory = loggerFactory;
            _parameters = parameters;
        }

        public string Name => "Consul";

        public IConfigurationTriggerSource StartListening(IReadOnlyConfigurationCallbackRegistry callbackRegistry)
        {
            if (_consulSubscriptionManager != null)
            {
                throw new InvalidOperationException("Cannot start listening because listening has been already started.");
            }

            _consulSubscriptionManager = new ConsulSubscriptionManager(_loggerFactory, _consulClientFactory(), callbackRegistry, _parameters);
            foreach (var callbackRegistryKey in callbackRegistry.Keys)
            {
                _consulSubscriptionManager.Subscribe(callbackRegistryKey);
            }

            return this;
        }

        public IConfigurationTriggerSource StopListening()
        {
            _consulSubscriptionManager?.Dispose();
            _consulSubscriptionManager = null;

            return this;
        }
    }
}
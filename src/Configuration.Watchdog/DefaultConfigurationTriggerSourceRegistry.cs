using System.Collections.Generic;
using System.Linq;
using Configuration.Watchdog.Abstraction;

namespace Configuration.Watchdog
{
    public class DefaultConfigurationTriggerSourceRegistry : IConfigurationTriggerSourceRegistry
    {
        private readonly Dictionary<string, IConfigurationTriggerSource> _triggerSources;

        public IReadOnlyCollection<IConfigurationTriggerSource> TriggerSources => _triggerSources.Values;

        public DefaultConfigurationTriggerSourceRegistry()
        {
            _triggerSources = new Dictionary<string, IConfigurationTriggerSource>();
        }

        public DefaultConfigurationTriggerSourceRegistry(IEnumerable<IConfigurationTriggerSource> configurationTriggerSources)
        {
            _triggerSources = configurationTriggerSources.ToDictionary(o => o.Name, o => o);
        }

        public IConfigurationTriggerSource GeTriggerSource(string name)
        {
            _triggerSources.TryGetValue(name, out var triggerSource);
            return triggerSource;
        }

        public IConfigurationTriggerSourceRegistry Add(IConfigurationTriggerSource configurationTriggerSource)
        {
            _triggerSources.Add(configurationTriggerSource.Name, configurationTriggerSource);

            return this;
        }
    }
}
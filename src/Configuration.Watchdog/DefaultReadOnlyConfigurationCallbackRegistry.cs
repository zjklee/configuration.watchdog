using System.Collections.Generic;
using Configuration.Watchdog.Abstraction;

namespace Configuration.Watchdog
{
    public class DefaultReadOnlyConfigurationCallbackRegistry : IReadOnlyConfigurationCallbackRegistry
    {
        private readonly IConfigurationCallbackRegistry _configurationCallbackRegistry;

        public DefaultReadOnlyConfigurationCallbackRegistry(IReadOnlyConfigurationCallbackRegistry configurationCallbackRegistry)
        {
            _configurationCallbackRegistry = new DefaultConfigurationCallbackRegistry();

            foreach (var key in configurationCallbackRegistry.Keys)
            {
                _configurationCallbackRegistry.Register(key, configurationCallbackRegistry.GetCallback(key));
            }
        }

        public IEnumerable<string> Keys => _configurationCallbackRegistry.Keys;
        public IConfigurationCallback GetCallback(string key) => _configurationCallbackRegistry.GetCallback(key);        
    }
}
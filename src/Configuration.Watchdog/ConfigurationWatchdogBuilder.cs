using System;
using System.Collections.Generic;
using System.Linq;
using Configuration.Watchdog.Abstraction;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace Configuration.Watchdog
{
    public class ConfigurationWatchdogBuilder : IConfigurationWatchDogBuilder
    {
        private IConfigurationCallbackRegistry _configurationCallbackRegistry;
        private IConfigurationTriggerSourceRegistry _configurationTriggerSourceRegistry;

        private readonly LinkedList<Action<IConfigurationCallbackRegistry>> _callbackRegistryConfigurationChain;
        private readonly LinkedList<Action<WatchdogConfiguration>> _watchDogConfigurationChain;

        private readonly LinkedList<Action<ConfigurationWatchDogParameters, IConfigurationTriggerSourceRegistry>>
            _sourceRegistryConfiguration;

        private ILoggerFactory _loggerFactory;

        public ConfigurationWatchdogBuilder()
        {           
            _callbackRegistryConfigurationChain = new LinkedList<Action<IConfigurationCallbackRegistry>>();
            _watchDogConfigurationChain = new LinkedList<Action<WatchdogConfiguration>>();
            _sourceRegistryConfiguration = new LinkedList<Action<ConfigurationWatchDogParameters, IConfigurationTriggerSourceRegistry>>();
        }

        public IConfigurationWatchDogBuilder WithCallbackRegistry(IConfigurationCallbackRegistry callbackRegistry)
        {
            _configurationCallbackRegistry = callbackRegistry;

            return this;
        }

        public IConfigurationWatchDogBuilder ConfigureCallbackRegistry(
            Action<IConfigurationCallbackRegistry> configuration)
        {
            _callbackRegistryConfigurationChain.AddLast(configuration);

            return this;
        }

        public IConfigurationWatchDogBuilder WithConfiguration(Action<WatchdogConfiguration> configuration)
        {
            _watchDogConfigurationChain.AddLast(configuration);

            return this;
        }

        public IConfigurationWatchDogBuilder ConfigureTriggerSources(Action<IConfigurationTriggerSourceRegistry> configure)
        {
            _sourceRegistryConfiguration.AddLast((parameters, registry) => configure(registry));

            return this;
        }

        public IConfigurationWatchDogBuilder ConfigureTriggerSources(Action<ConfigurationWatchDogParameters, IConfigurationTriggerSourceRegistry> configure)
        {
            _sourceRegistryConfiguration.AddLast(configure);

            return this;
        }

        public IConfigurationWatchDogBuilder WithLoggerFactory(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;

            return this;
        }        

        public IConfigurationWatchdog Build()
        {
            var loggerFactory = _loggerFactory ?? NullLoggerFactory.Instance;

            _configurationCallbackRegistry = _configurationCallbackRegistry 
                ?? new DefaultConfigurationCallbackRegistry();

            _configurationTriggerSourceRegistry = _configurationTriggerSourceRegistry 
                ?? new DefaultConfigurationTriggerSourceRegistry();

            var configurationCallbackRegistry = InvokeChain(_configurationCallbackRegistry, _callbackRegistryConfigurationChain);

            var configurationTriggerSourceRegistry = InvokeChain(new ConfigurationWatchDogParameters
                {
                    LoggerFactory = _loggerFactory
                }, 
                _configurationTriggerSourceRegistry, _sourceRegistryConfiguration);

            var watchDogConfiguration = InvokeChain(new WatchdogConfiguration
            {
                TriggerSourceStartAttemptsCount = 0
            }, _watchDogConfigurationChain);

            return new ConfigurationWatchdog(new ConfigurationWatchdogCreationParameters
            {
                LoggerFactory = loggerFactory,
                ConfigurationCallbackRegistry = configurationCallbackRegistry,
                ConfigurationTriggerSourceRegistry = configurationTriggerSourceRegistry,
                WatchdogConfiguration = watchDogConfiguration
            });
        }

        private static T InvokeChain<T>(T initialValue, LinkedList<Action<T>> actions) where T: class 
        {
            return actions.Aggregate(initialValue, (o, action) =>
            {
                action(o);
                return o;
            });
        }

        private static T InvokeChain<T, TShared>(TShared shared, T initialValue, LinkedList<Action<TShared, T>> actions) 
            where T : class
        {
            return actions.Aggregate(initialValue, (o, action) =>
            {
                action(shared, o);
                return o;
            });
        }
    }
}
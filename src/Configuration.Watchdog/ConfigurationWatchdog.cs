﻿using System;
using System.Collections.Concurrent;
using Configuration.Watchdog.Abstraction;
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog
{
    internal class ConfigurationWatchdog : IConfigurationWatchdog
    {
        private readonly IReadOnlyConfigurationCallbackRegistry _configurationCallbackRegistry;
        private readonly IReadOnlyConfigurationTriggerSourceRegistry _configurationTriggerSourceRegistry;

        private readonly ILogger _logger;

        private readonly ConcurrentQueue<IConfigurationTriggerSource> _runningTriggerSources;

        private readonly ConcurrentQueue<IConfigurationTriggerSource> _failedTriggerSources;

        private readonly WatchdogConfiguration _watchdogConfiguration;

        public ConfigurationWatchdog(ConfigurationWatchdogCreationParameters parameters)
        {
            _runningTriggerSources = new ConcurrentQueue<IConfigurationTriggerSource>();
            _failedTriggerSources = new ConcurrentQueue<IConfigurationTriggerSource>();

            _configurationCallbackRegistry = new DefaultReadOnlyConfigurationCallbackRegistry(parameters.ConfigurationCallbackRegistry);
            _configurationTriggerSourceRegistry = new DefaultReadOnlyConfigurationTriggerSourceRegistry(parameters.ConfigurationTriggerSourceRegistry);

            _watchdogConfiguration = parameters.WatchdogConfiguration;

            _logger = parameters.LoggerFactory.CreateLogger(typeof(ConfigurationWatchdog).Name);
        }

        public IConfigurationWatchdog StartListening()
        {
            foreach (var configurationTriggerSource in _configurationTriggerSourceRegistry.TriggerSources)
            {
                using (_logger.BeginScope($"Starting {configurationTriggerSource.GetType().Name} source"))
                {
                    var attempt = 0;
                    while (true)                                              
                    {
                        try
                        {
                            var source = configurationTriggerSource.StartListening(_configurationCallbackRegistry);
                            _runningTriggerSources.Enqueue(source);
                            _logger.LogInformation("Source started");
                            break;
                        }
                        catch (Exception e)
                        {                        
                            if (attempt >= _watchdogConfiguration.TriggerSourceStartAttemptsCount)
                            {
                                _logger.LogError(attempt, e, "Failed to start source.");
                                _failedTriggerSources.Enqueue(configurationTriggerSource);
                                break;
                            }

                            _logger.LogError(attempt, e, "Failed to start source. Reattempting.");
                            attempt++;
                        }
                    }
                }
            }

            return this;
        }

        public IConfigurationWatchdog StopListening()
        {
            while (_runningTriggerSources.TryDequeue(out var source))
            {
                using (_logger.BeginScope($"Stopping {source.GetType().Name} source"))
                {
                    source.StopListening();
                }
            }            

            return this;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Configuration.Watchdog.Abstraction;

namespace Configuration.Watchdog
{
    public class DefaultConfigurationCallbackRegistry : IConfigurationCallbackRegistry
    {
        private readonly Dictionary<string, IConfigurationCallback> _callbacks;

        public IEnumerable<string> Keys => _callbacks.Keys;

        public DefaultConfigurationCallbackRegistry()
        {
            _callbacks = new Dictionary<string, IConfigurationCallback>();
        }        

        public IConfigurationCallback GetCallback(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentNullException(nameof(key));

            IConfigurationCallback callback;
            _callbacks.TryGetValue(key, out callback);
            return callback;
        }

        public IConfigurationCallbackRegistry Register(string key, IConfigurationCallback callback)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentNullException(nameof(key));
            if (callback == null) throw new ArgumentNullException(nameof(callback));

            _callbacks[key] = callback;

            return this;
        }

        public IConfigurationCallbackRegistry Register(string key, Func<Task> callback)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentNullException(nameof(key));
            if (callback == null) throw new ArgumentNullException(nameof(callback));

            _callbacks[key] = new DelegateConfigurationCallback(parameters => callback());

            return this;
        }

        public IConfigurationCallbackRegistry Register(string key, Action callback)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentNullException(nameof(key));
            if (callback == null) throw new ArgumentNullException(nameof(callback));

            _callbacks[key] = new DelegateConfigurationCallback(parameters => callback());

            return this;
        }       
    }
}
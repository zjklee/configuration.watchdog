using Configuration.Watchdog.Abstraction;
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog
{
    internal class ConfigurationWatchdogCreationParameters
    {
        public IConfigurationCallbackRegistry ConfigurationCallbackRegistry { get; set; }
        public IConfigurationTriggerSourceRegistry ConfigurationTriggerSourceRegistry { get; set; }
        public WatchdogConfiguration WatchdogConfiguration { get; set; }
        public ILoggerFactory LoggerFactory { get; set; }
    }
}
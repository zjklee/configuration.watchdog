using System.Collections.Generic;
using Configuration.Watchdog.Abstraction;

namespace Configuration.Watchdog
{
    public class DefaultReadOnlyConfigurationTriggerSourceRegistry : IReadOnlyConfigurationTriggerSourceRegistry
    {
        private readonly IReadOnlyConfigurationTriggerSourceRegistry _configurationTriggerSourceRegistry;

        public DefaultReadOnlyConfigurationTriggerSourceRegistry(IReadOnlyConfigurationTriggerSourceRegistry configurationTriggerSourceRegistry)
        {
            _configurationTriggerSourceRegistry = new DefaultConfigurationTriggerSourceRegistry(configurationTriggerSourceRegistry.TriggerSources);            
        }

        public IReadOnlyCollection<IConfigurationTriggerSource> TriggerSources => 
            _configurationTriggerSourceRegistry.TriggerSources;        
    }
}
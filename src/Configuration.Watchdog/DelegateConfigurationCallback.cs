using System;
using System.Threading.Tasks;
using Configuration.Watchdog.Abstraction;

namespace Configuration.Watchdog
{
    public class DelegateConfigurationCallback : IConfigurationCallback
    {
        public struct Parameters
        {
            public string Key { get; set; }
            public string OldValue { get; set; }
            public string NewValue { get; set; }
        }

        private readonly Func<Parameters, Task> _delegate;

        public DelegateConfigurationCallback(Func<Parameters, Task> action)
        {
            _delegate = action;
        }

        public DelegateConfigurationCallback(Action<Parameters> action)
        {
            _delegate = parameters =>
            {
                action(parameters);
                return Task.FromResult(true);
            };
        }

        public Task Invoke(string key, string oldValue, string newValue)
        {
            return _delegate(new Parameters
            {
                Key = key,
                NewValue = newValue,
                OldValue = oldValue
            });
        }
    }
}
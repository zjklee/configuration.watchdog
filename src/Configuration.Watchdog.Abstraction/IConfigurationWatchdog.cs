﻿namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationWatchdog
    {        
        IConfigurationWatchdog StartListening();
        IConfigurationWatchdog StopListening();
    }
}
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog.Abstraction
{
    public class ConfigurationWatchDogParameters
    {
        public ILoggerFactory LoggerFactory { get; set; }
    }
}
namespace Configuration.Watchdog.Abstraction
{    
    public class WatchdogConfiguration
    {
        public int TriggerSourceStartAttemptsCount { get; set; }
    }
}
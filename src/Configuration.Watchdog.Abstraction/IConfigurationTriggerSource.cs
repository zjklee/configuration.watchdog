﻿namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationTriggerSource
    {        
        string Name { get; }

        IConfigurationTriggerSource StartListening(IReadOnlyConfigurationCallbackRegistry callbackRegistry);
        IConfigurationTriggerSource StopListening();
    }
}
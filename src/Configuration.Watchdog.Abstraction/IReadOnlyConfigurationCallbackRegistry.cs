using System.Collections.Generic;

namespace Configuration.Watchdog.Abstraction
{
    public interface IReadOnlyConfigurationCallbackRegistry
    {
        IEnumerable<string> Keys { get; }
        IConfigurationCallback GetCallback(string key);
    }
}
using System.Collections.Generic;

namespace Configuration.Watchdog.Abstraction
{
    public interface IReadOnlyConfigurationTriggerSourceRegistry
    {
        IReadOnlyCollection<IConfigurationTriggerSource> TriggerSources { get; }
    }
}
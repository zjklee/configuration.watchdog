using System.Threading.Tasks;

namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationCallback
    {        
        Task Invoke(string key, string oldValue, string newValue);
    }
}
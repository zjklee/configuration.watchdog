﻿using System;
using Microsoft.Extensions.Logging;

namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationWatchDogBuilder
    {
        IConfigurationWatchDogBuilder WithCallbackRegistry(IConfigurationCallbackRegistry callbackRegistry);
        IConfigurationWatchDogBuilder ConfigureCallbackRegistry(Action<IConfigurationCallbackRegistry> configuration);
        IConfigurationWatchDogBuilder WithLoggerFactory(ILoggerFactory loggerFactory);
        IConfigurationWatchDogBuilder WithConfiguration(Action<WatchdogConfiguration> configuration);        
        IConfigurationWatchDogBuilder ConfigureTriggerSources(Action<IConfigurationTriggerSourceRegistry> configure);
        IConfigurationWatchDogBuilder ConfigureTriggerSources(Action<ConfigurationWatchDogParameters, IConfigurationTriggerSourceRegistry> configure);
        IConfigurationWatchdog Build();
    }
}
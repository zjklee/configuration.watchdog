﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Configuration.Watchdog.Abstraction
{
    public interface ISubscriptionManager : IDisposable
    {
        void Subscribe(string key);
        IReadOnlyDictionary<string, Task> Subscriptions { get; }
    }
}
namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationTriggerSourceRegistry : IReadOnlyConfigurationTriggerSourceRegistry
    {
        IConfigurationTriggerSource GeTriggerSource(string name);
        IConfigurationTriggerSourceRegistry Add(IConfigurationTriggerSource configurationTriggerSource);
    }
}
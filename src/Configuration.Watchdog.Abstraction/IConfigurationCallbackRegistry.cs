﻿using System;
using System.Threading.Tasks;

namespace Configuration.Watchdog.Abstraction
{
    public interface IConfigurationCallbackRegistry : IReadOnlyConfigurationCallbackRegistry
    {
        IConfigurationCallbackRegistry Register(string key, IConfigurationCallback callback);
        IConfigurationCallbackRegistry Register(string key, Func<Task> callback);
        IConfigurationCallbackRegistry Register(string key, Action callback);
    }
}
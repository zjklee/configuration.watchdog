﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Configuration.Watchdog.Abstraction;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Configuration.Watchdog.Consul.Tests
{
    public class ConsulConfigurationWatchDogTests
    {
        private const string ExpectedKey = "key";
        private const string ExpectedOldValue = "value";
        private const string ExpectedNewValue = "newvalue";
        private const string PrefixWithSlash = "prefix/";
        private const string ExpectedKeyWithPrefix = "prefix/key";
        private const int CreateIndex = 1;
        private const int UpdateIndex = 2;
        private const int QueueDefaultDelay = 100;

        private Mock<IConsulClient> _consulClient;
        private Mock<IKVEndpoint> _kvEndpoint;
        private Mock<IConfigurationCallback> _consulConfigurationCallback;
        private Mock<ILoggerFactory> _loggerFactory;
        private Mock<ILogger> _logger;
        private Mock<IConfigurationRoot> _configurationRoot;

        private IConfigurationWatchDogBuilder _consulConfigurationWatchDogBuilder;
        private ConcurrentQueue<QueryResult<KVPair>> _concurrentQueue;
        private List<string> _messages;

        public ConsulConfigurationWatchDogTests()
        {
            _consulClient = new Mock<IConsulClient>();
            _kvEndpoint = new Mock<IKVEndpoint>();
            _loggerFactory = new Mock<ILoggerFactory>();
            _logger = new Mock<ILogger>();
            _configurationRoot = new Mock<IConfigurationRoot>();
            _consulConfigurationCallback = new Mock<IConfigurationCallback>();

            _messages = new List<string>();
            _logger.Setup(o => o.Log(It.IsAny<LogLevel>(), It.IsAny<EventId>(), It.IsAny<object>(),
                    It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()))
                .Callback<LogLevel, EventId, object, Exception, Func<object, Exception, string>>((level, eventId, state,
                    exception, formatter) =>
                {
                    _messages.Add(state.ToString());
                });

            _kvEndpoint.Setup(o => o.Get(It.IsAny<string>(), It.IsAny<QueryOptions>(), It.IsAny<CancellationToken>()))
                .Returns<string, QueryOptions, CancellationToken>(
                    async (key, options, token) => await TaskMethod(token));

            _kvEndpoint.Setup(o => o.Get(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Returns<string, CancellationToken>(async (key, token) => await TaskMethod(token));

            _consulClient.SetupGet(o => o.KV).Returns(() => _kvEndpoint.Object);

            _loggerFactory.Setup(o => o.CreateLogger(It.IsAny<string>())).Returns(() => _logger.Object);

            _consulConfigurationWatchDogBuilder = new ConfigurationWatchdogBuilder();

            _concurrentQueue = new ConcurrentQueue<QueryResult<KVPair>>();
        }

        [Fact]
        public async Task Should_Make_Request_Without_Prefix()
        {
            var watchdog = _consulConfigurationWatchDogBuilder
                .AddConsul(() => _consulClient.Object)
                .ConfigureCallbackRegistry(
                    registry => registry.Register(ExpectedKey, _consulConfigurationCallback.Object)
                )
                .Build()
                .StartListening();

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKey, CreateIndex, ExpectedOldValue, delay: 500);

            _kvEndpoint.Verify(o => o.Get(It.Is<string>(s => s.Equals(ExpectedKey)), It.IsAny<CancellationToken>()),
                Times.Once);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKey, UpdateIndex, ExpectedNewValue, delay: 500);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKey)),
                    It.Is<QueryOptions>(options => options.WaitIndex == UpdateIndex),
                    It.IsAny<CancellationToken>()), Times.Once);

            _consulConfigurationCallback.Verify(o => o.Invoke(
                    It.Is<string>(key => key.Equals(ExpectedKey)),
                    It.Is<string>(oldValue => oldValue.Equals(ExpectedOldValue)),
                    It.Is<string>(newValue => newValue.Equals(ExpectedNewValue))),
                Times.Once);

            watchdog.StopListening();
        }

        [Fact]
        public async Task Should_Fire_Configuration_Reload()
        {
            var watchdog = _consulConfigurationWatchDogBuilder
                .AddConsul(() => _consulClient.Object)
                .ConfigureCallbackRegistry(registry =>
                {
                    registry.Register(ExpectedKey, () => _configurationRoot.Object.Reload());
                })
                .Build()
                .StartListening();

            _configurationRoot.Verify(o => o.Reload(), Times.Never);

            await Enqueue(ExpectedKey, CreateIndex, ExpectedOldValue, delay: 200);

            _kvEndpoint.Verify(o => o.Get(It.Is<string>(s => s.Equals(ExpectedKey)), It.IsAny<CancellationToken>()),
                Times.Once);

            _configurationRoot.Verify(o => o.Reload(), Times.Never);

            await Enqueue(ExpectedKey, UpdateIndex, ExpectedNewValue, delay: 200);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKey)),
                    It.Is<QueryOptions>(options => options.WaitIndex == UpdateIndex),
                    It.IsAny<CancellationToken>()), Times.Once);

            _configurationRoot.Verify(o => o.Reload(), Times.Once);

            watchdog.StopListening();
        }

        [Fact]
        public async Task Should_Make_Request_With_Prefix()
        {
            var watchdog = _consulConfigurationWatchDogBuilder
                .AddConsul(parameters => { parameters.Prefix = PrefixWithSlash; }, () => _consulClient.Object)
                .ConfigureCallbackRegistry(
                    registry => registry.Register(ExpectedKey, _consulConfigurationCallback.Object))
                .Build()
                .StartListening();

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, CreateIndex, ExpectedOldValue, delay: 1000);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)), It.IsAny<CancellationToken>()),
                Times.Once);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, UpdateIndex, ExpectedNewValue, delay: 500);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)),
                    It.Is<QueryOptions>(options => options.WaitIndex == UpdateIndex),
                    It.IsAny<CancellationToken>()), Times.Once);

            _consulConfigurationCallback.Verify(o => o.Invoke(
                    It.Is<string>(key => key.Equals(ExpectedKey)),
                    It.Is<string>(oldValue => oldValue.Equals(ExpectedOldValue)),
                    It.Is<string>(newValue => newValue.Equals(ExpectedNewValue))),
                Times.Once);

            watchdog.StopListening();
        }

        [Fact]
        public async Task Should_Log_If_Callback_Fails()
        {
            _consulConfigurationCallback
                .Setup(o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(() => Task.Run(() => { throw new InvalidOperationException("Callback fails"); }));

            var watchdog = _consulConfigurationWatchDogBuilder
                .ConfigureTriggerSources((parameters, registry) =>
                {
                    registry.Add(
                        new ConsulConfigurationTriggerSourceBuilder()
                            .WithPrefix(PrefixWithSlash)
                            .WithLoggerFactory(parameters.LoggerFactory)
                            .WithConsulClientFactory(() => _consulClient.Object)
                            .Build()
                    );
                })
                .WithLoggerFactory(_loggerFactory.Object)
                .ConfigureCallbackRegistry(
                    registry => registry.Register(ExpectedKey, _consulConfigurationCallback.Object))
                .Build().StartListening();


            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, CreateIndex, ExpectedOldValue);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, UpdateIndex, ExpectedNewValue);

            _consulConfigurationCallback.Verify(o => o.Invoke(
                    It.Is<string>(key => key.Equals(ExpectedKey)),
                    It.Is<string>(oldValue => oldValue.Equals(ExpectedOldValue)),
                    It.Is<string>(newValue => newValue.Equals(ExpectedNewValue))),
                Times.Once);

            Assert.Contains("Callback handler failed for key 'key'", _messages);

            watchdog.StopListening();
        }

        [Fact]
        public async Task Should_Not_Fire_Event_If_Same_Index()
        {
            var watchdog = _consulConfigurationWatchDogBuilder
                .AddConsul(parameters => { parameters.Prefix = PrefixWithSlash; }, () => _consulClient.Object)
                .ConfigureCallbackRegistry(
                    registry => registry.Register(ExpectedKey, _consulConfigurationCallback.Object))
                .Build()
                .StartListening();

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, CreateIndex, ExpectedOldValue);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)), It.IsAny<CancellationToken>()),
                Times.Once);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, CreateIndex, ExpectedOldValue);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)),
                    It.Is<QueryOptions>(options => options.WaitIndex == CreateIndex),
                    It.IsAny<CancellationToken>()), Times.AtLeastOnce);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            watchdog.StopListening();
        }

        [Fact]
        public async Task Should_Not_Fire_Event_If_Same_Value()
        {
            var watchdog = _consulConfigurationWatchDogBuilder
                .AddConsul(parameters => { parameters.Prefix = PrefixWithSlash; }, () => _consulClient.Object)
                .ConfigureCallbackRegistry(
                    registry => registry.Register(ExpectedKey, _consulConfigurationCallback.Object))
                .Build()
                .StartListening();

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, CreateIndex, ExpectedOldValue);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)), It.IsAny<CancellationToken>()),
                Times.Once);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            await Enqueue(ExpectedKeyWithPrefix, UpdateIndex, ExpectedOldValue);

            _kvEndpoint.Verify(
                o => o.Get(It.Is<string>(s => s.Equals(ExpectedKeyWithPrefix)),
                    It.Is<QueryOptions>(options => options.WaitIndex == UpdateIndex),
                    It.IsAny<CancellationToken>()), Times.Once);

            _consulConfigurationCallback.Verify(
                o => o.Invoke(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()),
                Times.Never);

            watchdog.StopListening();
        }        

        private Task<QueryResult<KVPair>> TaskMethod(CancellationToken token)
        {
            return Task.Run(async () =>
            {
                QueryResult<KVPair> value;
                while (!_concurrentQueue.TryDequeue(out value))
                {
                    await Task.Delay(10, token);
                }

                return value;
            }, token);
        }

        private async Task Enqueue(string key, ulong index, string value, HttpStatusCode statusCode = HttpStatusCode.OK,
            int delay = QueueDefaultDelay)
        {
            _concurrentQueue.Enqueue(new QueryResult<KVPair>
            {
                Response = new KVPair(key)
                {
                    ModifyIndex = index,
                    CreateIndex = CreateIndex,
                    Value = Encoding.UTF8.GetBytes(value)
                },
                StatusCode = statusCode
            });

            await Task.Delay(delay);
        }
    }
}
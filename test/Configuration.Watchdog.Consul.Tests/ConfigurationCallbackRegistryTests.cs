﻿using System;
using System.Threading.Tasks;
using Configuration.Watchdog.Abstraction;
using FluentAssertions;
using Xunit;

namespace Configuration.Watchdog.Consul.Tests
{
    public class ConfigurationCallbackRegistryTests
    {
        private const string ExpectedKey = "key";
        private const string ExpectedNewValue = "newValue";
        private const string ExpectedOldValue = "oldValue";
        private readonly IConfigurationCallbackRegistry _consulConfigurationCallbackRegistry;
        private int _counter;

        public ConfigurationCallbackRegistryTests()
        {
            _consulConfigurationCallbackRegistry = new DefaultConfigurationCallbackRegistry();

            _counter = 0;
        }

        private class Callback : IConfigurationCallback
        {
            private readonly string _key;
            private readonly string _oldValue;
            private readonly string _newValue;
            private readonly Action _callback;

            public Callback(string key, string oldValue, string newValue, Action callback)
            {
                _key = key;
                _oldValue = oldValue;
                _newValue = newValue;
                _callback = callback;
            }            

            public Task Invoke(string key, string oldValue, string newValue)
            {
                key.Should().Be(_key);
                oldValue.Should().Be(_oldValue);
                newValue.Should().Be(_newValue);
                _callback();

                return Task.FromResult(true);
            }
        }

        [Fact]
        public async Task Should_Add_Callback_Using_Interface_Implementation()
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, new Callback(ExpectedKey, ExpectedOldValue, ExpectedNewValue, () => _counter++));

            _consulConfigurationCallbackRegistry.Keys.Should().Contain(ExpectedKey);
            await _consulConfigurationCallbackRegistry.GetCallback(ExpectedKey).Invoke(ExpectedKey, ExpectedOldValue, ExpectedNewValue);
            _counter.Should().Be(1);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Should_Throw_When_Key_Is_Null_Or_Whitespace(string key)
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, new Callback(ExpectedKey, ExpectedOldValue, ExpectedNewValue, () => _counter++));

            Action action = () => _consulConfigurationCallbackRegistry.GetCallback(key);
            var exception = Assert.Throws<ArgumentNullException>(action);
            exception.ParamName.Should().Be("key");
        }

        [Fact]
        public async Task Should_Add_Callback_Using_Action_Without_Parameters()
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, () => _counter++);

            _consulConfigurationCallbackRegistry.Keys.Should().Contain(ExpectedKey);
            await _consulConfigurationCallbackRegistry.GetCallback(ExpectedKey).Invoke(ExpectedKey, ExpectedOldValue, ExpectedNewValue);
            _counter.Should().Be(1);
        }

        [Fact]
        public async Task Should_Add_Callback_Using_Action_With_Parameters()
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, new DelegateConfigurationCallback(parameters =>
            {
                _counter++;
                parameters.NewValue.Should().Be(ExpectedNewValue);
            }));

            _consulConfigurationCallbackRegistry.Keys.Should().Contain(ExpectedKey);
            await _consulConfigurationCallbackRegistry.GetCallback(ExpectedKey).Invoke(ExpectedKey, ExpectedOldValue, ExpectedNewValue);
            _counter.Should().Be(1);
        }

        [Fact]
        public async Task Should_Add_Callback_Using_Func_Without_Parameters()
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, () => Task.FromResult(_counter++));

            _consulConfigurationCallbackRegistry.Keys.Should().Contain(ExpectedKey);
            await _consulConfigurationCallbackRegistry.GetCallback(ExpectedKey).Invoke(ExpectedKey, ExpectedOldValue, ExpectedNewValue);
            _counter.Should().Be(1);
        }

        [Fact]
        public async Task Should_Add_Callback_Using_Func_With_Parameters()
        {
            _consulConfigurationCallbackRegistry.Register(ExpectedKey, new DelegateConfigurationCallback(parameters =>
            {
                parameters.NewValue.Should().Be(ExpectedNewValue);
                return Task.FromResult(_counter++);
            }));

            _consulConfigurationCallbackRegistry.Keys.Should().Contain(ExpectedKey);
            await _consulConfigurationCallbackRegistry.GetCallback(ExpectedKey).Invoke(ExpectedKey, ExpectedOldValue, ExpectedNewValue);
            _counter.Should().Be(1);
        }

        [Theory]
        [InlineData(null, null, "key")]
        [InlineData(null, "key", "callback")]
        public void Should_Throw_If_Arguments_Are_Null_Using_Interface_Implementation(IConfigurationCallback callback, string key, string expectedParameterName)
        {
            Action action = () => _consulConfigurationCallbackRegistry.Register(key, callback);
            var exception = Assert.Throws<ArgumentNullException>(action);
            exception.ParamName.Should().Be(expectedParameterName);
        }

        [Theory]
        [InlineData(null, null, "key")]
        [InlineData(null, "key", "callback")]
        public void Should_Throw_If_Arguments_Are_Null_Using_Action_Without_Parameters(Action callback, string key, string expected)
        {
            Action action = () => _consulConfigurationCallbackRegistry.Register(key, callback);
            var exception = Assert.Throws<ArgumentNullException>(action);
            exception.ParamName.Should().Be(expected);
        }

        [Theory]
        [InlineData(null, null, "key")]
        [InlineData(null, "key", "callback")]
        public void Should_Throw_If_Arguments_Are_Null_Using_Func_Without_Parameters(Func<Task> callback, string key, string expected)
        {
            Action action = () => _consulConfigurationCallbackRegistry.Register(key, callback);
            var exception = Assert.Throws<ArgumentNullException>(action);
            exception.ParamName.Should().Be(expected);
        }

        [Theory]
        [InlineData(null, null, "key")]
        [InlineData(null, "key", "callback")]
        public void Should_Throw_If_Arguments_Are_Null_Using_Action_With_Parameters(DelegateConfigurationCallback callback, string key, string expected)
        {
            Action action = () => _consulConfigurationCallbackRegistry.Register(key, callback);
            var exception = Assert.Throws<ArgumentNullException>(action);
            exception.ParamName.Should().Be(expected);
        }                
    }
}
